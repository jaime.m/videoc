const myVideo= document.createElement('video');
myVideo.muted=true;
const socket =io('/');
const videoGrid = document.getElementById('video-grid');
//undefined is the id, but it is create automaticaly by peer.
let peer = new Peer(undefined,{
    path:'/peerjs',
    host:'/',
    port:'3030'
});

let myVideoStream;
navigator.mediaDevices.getUserMedia({
    video:true,
    audio:true,
}).then((stream)=>{
    addVideoStream(myVideo,stream);

peer.on('call', call=>{
    call.answer(stream);
    const video = document.createElement('video');
    call.on('stream',userVideoStream=>{
        addVideoStream(video,userVideoStream);
    })
});

socket.on('user-connected',userId=>{
    connecToNewUser(userId,stream);
});
    
});

//Listen peer connection
peer.on('open',id=>{
    socket.emit('join-room',ROOM_ID,id);
});

const connecToNewUser= (userId,stream)=>{
    const call = peer.call(userId,stream);
    peer.emit('call');
    const video = document.createElement('video');
    call.on('stream', userVideoStream=>{
        console.log('streaming');
        addVideoStream(video,userVideoStream);
    }); 
};

const addVideoStream=(video,stream)=>{
    console.log('creating video instance');
    video.srcObject=stream;
    video.addEventListener('loadedmetadata',()=>{
        video.play();
    });
    videoGrid.append(video);
};